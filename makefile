# Makefile for Writing Make Files Example

# *****************************************************
# Variables to control Makefile operation

CXX = g++
CXXFLAGS = -Wall -g

# ****************************************************
# Targets needed to bring the executable up to date

main: main.o matrix.o vector.o
	$(CXX) $(CXXFLAGS) -o main main.o matrix.o vector.o

# The main.o target can be written more simply

main.o: main.cc matrix.h vector.h
	$(CXX) $(CXXFLAGS) -c main.cc

matrix.o: matrix.h

vector.o: vector.h matrix.h
