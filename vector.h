#ifndef _VECTOR_H
#define _VECTOR_H

#include "matrix.h"



class vec
{
 int b[ROW * COL];
 int row_v;
 int col_v;
 public:
 		vec () : row_v(0), col_v(0)
 		{}
  		void vec_read(int r1,int c1);
  		void vec_display(matrix m1, vec v1);
  		vec vec_multiply (matrix m1, vec v1);
};
#endif
