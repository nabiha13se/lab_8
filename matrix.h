#ifndef _MATRIX_H
#define _MATRIX_H


const int ROW=10;
const int COL=10;

class matrix
{
 
 
 public:
	 int a[ROW * COL];
	 int row;
	 int col;
 		matrix () : row(0), col(0)
 		{}
  		void read(int r, int c);
  		void display(matrix m1, matrix m2);
  		matrix multiply (matrix m1, matrix m2);
};
#endif
